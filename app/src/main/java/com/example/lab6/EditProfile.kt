package com.example.lab6

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.lab6.utils.Mockups

class EditProfile : AppCompatActivity() {

    private lateinit var usernameEditText: EditText
    private lateinit var userImage: ImageView
    private lateinit var userCoverImage: ImageView
    private lateinit var cancelEditButton: Button
    private lateinit var updateProfileButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        usernameEditText = findViewById(R.id.username_edit_text)

        userImage = findViewById(R.id.edit_profile_user_image)
        userCoverImage = findViewById(R.id.edit_profile_user_cover_image)
        cancelEditButton = findViewById(R.id.cancel_edit_profile_button)
        updateProfileButton = findViewById(R.id.update_profile_button)

        updateProfileButton.setOnClickListener(mOnButtonClickListener)
        cancelEditButton.setOnClickListener(mOnButtonClickListener)

        usernameEditText.setText(Mockups.ownName)

        userCoverImage.setOnClickListener{
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                pickImageFromGallery()
            }
        }
        userImage.setOnClickListener {
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED){
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                }
                else{
                    //permission already granted
                    pickImageFromGallery();
                }
            }
            else{
                pickImageFromGallery()
            }
        }

    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    pickImageFromGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }



    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }

    private var mOnButtonClickListener = View.OnClickListener { item->
        when(item.id){
            R.id.update_profile_button -> {
                Mockups.ownName = usernameEditText.text.toString()
                val intent = Intent(this, OwnProfileActivity::class.java)
                startActivity(intent)
            }
            R.id.cancel_edit_profile_button -> {
                val intent = Intent(this, OwnProfileActivity::class.java)
                startActivity(intent)
            }
        }
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            userImage.setImageURI(data?.data)
        }
    }
}