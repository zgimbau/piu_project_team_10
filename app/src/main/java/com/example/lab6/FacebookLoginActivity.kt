package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import java.util.*
import kotlin.concurrent.schedule

class FacebookLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facebook_login)

        Timer("FacebookSignUp", false).schedule(2000){
            Handler(mainLooper).post{
                val intent = Intent(this@FacebookLoginActivity, FeedActivity::class.java)
                startActivity(intent)
            }
        }
    }
}