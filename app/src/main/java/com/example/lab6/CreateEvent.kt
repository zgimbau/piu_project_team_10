package com.example.lab6

import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.lab6.utils.Mockups

class CreateEvent : AppCompatActivity() {

    private lateinit var map: ImageView
    private lateinit var tools: ImageView
    private lateinit var toolsClick: TextView
    private lateinit var name: EditText
    private lateinit var description: EditText
    private lateinit var create: Button
    private lateinit var back: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_event)

        map = findViewById(R.id.map)
        tools = findViewById(R.id.tools_img)
        toolsClick = findViewById(R.id.tools_click)
        name = findViewById(R.id.name_text)
        description = findViewById(R.id.description_text)
        create = findViewById(R.id.create_button)
        back = findViewById(R.id.back_button)


        map.setOnClickListener() {
            val intent = Intent(this, BookLocationTimeActivity::class.java)
            startActivity(intent)
        }

        toolsClick.setOnClickListener() {
            val intent = Intent(this, Equipment::class.java)
            startActivity(intent)

            toolsClick.isVisible = false
            tools.isVisible = true
        }
        create.setOnClickListener() {
            Toast.makeText(this, "Event created!", Toast.LENGTH_SHORT).show()
            Mockups.newEventName = name.getText().toString()
            Mockups.showNewEvent = 1
            val intent = Intent(this, OwnProfileActivity::class.java)
            startActivity(intent)
        }
        back.setOnClickListener() {
            val intent = Intent(this, OwnProfileActivity::class.java)
            startActivity(intent)
        }
    }
}