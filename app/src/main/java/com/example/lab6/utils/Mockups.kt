package com.example.lab6.utils

object Mockups {
    var own_activities: IntArray = intArrayOf(1,1,0,1,0,0)
    var ownName = "Ion Ionescu"
    var owner_event: Int=1
    var eventName:String="Basket tras la cos"
    var eventDescription:String=""
    var goingInterestedEvent: IntArray = intArrayOf(0,0)
    var ownEventDeleted = false

    //New user
    var newUserUsername = ""
    var newUserPassword = ""
    var newUserFullName = ""

    //NewEvent
    var newEventName = ""
    var showNewEvent = 0
}