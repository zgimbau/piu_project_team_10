package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class Chat : AppCompatActivity() {

    private lateinit var backImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        backImage = findViewById(R.id.back_image)
        backImage.setOnClickListener {
            val intent = Intent(this, Friends::class.java)
            startActivity(intent)
        }
    }
}