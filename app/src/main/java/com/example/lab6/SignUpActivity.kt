package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.lab6.utils.Mockups

class SignUpActivity : AppCompatActivity() {

    private lateinit var signUpButton: Button
    private lateinit var fullNameEditText: EditText
    private lateinit var usernameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var confirmPasswordEditText: EditText
    private lateinit var incompleteFieldsWarning: TextView
    private lateinit var confirmPasswordError: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        signUpButton = findViewById(R.id.signUpForRealButton)
        fullNameEditText = findViewById(R.id.fullNameText)
        usernameEditText = findViewById(R.id.usernameText)
        passwordEditText = findViewById(R.id.passwordText)
        incompleteFieldsWarning = findViewById(R.id.incompleteFieldsWarning)
        confirmPasswordEditText = findViewById(R.id.confirm_passwordText)
        confirmPasswordError = findViewById(R.id.confirmPasswordErrorLabel)

        signUpButton.setOnClickListener(mOnSignUpClick)

    }

    private var mOnSignUpClick = View.OnClickListener { item ->
        when (item.id) {
            R.id.signUpForRealButton -> {
                val fullName = fullNameEditText.text.toString()
                val username = usernameEditText.text.toString()
                val password = passwordEditText.text.toString()
                val confirmPassword = confirmPasswordEditText.text.toString()
                if (fullName.isEmpty() || username.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
                    incompleteFieldsWarning.visibility = View.VISIBLE
                } else {
                    if (!password.equals(confirmPassword)) {
                        confirmPasswordError.visibility = View.VISIBLE
                    } else {
                        Mockups.newUserFullName = fullName
                        Mockups.newUserPassword = password
                        Mockups.newUserUsername = username
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }
}