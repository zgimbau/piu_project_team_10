package com.example.lab6

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.lab6.utils.Mockups

class EventViewPageActivity: AppCompatActivity(), AdapterView.OnItemClickListener {
    private lateinit var goingButton: Button
    private lateinit var shareButton: Button
    private lateinit var interestedButton: Button

    private lateinit var vec1: IntArray
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.event_view_page)

        goingButton=findViewById(R.id.going_button)
        shareButton=findViewById(R.id.share_button)
        interestedButton=findViewById(R.id.interested_button)
        vec1 = intArrayOf(0,0)

        if(Mockups.goingInterestedEvent[0]==0){
            goingButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.checkbox_off_background,0);
          
        }
        else{
            goingButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.checkbox_on_background,0);
        }
        if(Mockups.goingInterestedEvent[1]==0){
            interestedButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.star_big_off,0);
        }else{
            interestedButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.star_big_on,0);
        }
        goingButton.setOnClickListener(){
            if(vec1[0] == 0){
                goingButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.checkbox_on_background,0);
                vec1[0] = 1
                Mockups.goingInterestedEvent[0]=1
            }
            else{
                goingButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.checkbox_off_background,0);
                vec1[0] = 0
                Mockups.goingInterestedEvent[0]=0
            }
        }
        shareButton.setOnClickListener(){
            Toast.makeText(this,"The event was shared !", Toast.LENGTH_SHORT).show()
        }
        interestedButton.setOnClickListener(){
            if(vec1[1] == 0){
                interestedButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.star_big_on,0);
                vec1[1] = 1
                Mockups.goingInterestedEvent[1]=1
            }
            else{
                interestedButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.star_big_off,0);
                vec1[1] = 0
                Mockups.goingInterestedEvent[0]=0
            }
        }

    }
    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("Not yet implemented")
    }
}