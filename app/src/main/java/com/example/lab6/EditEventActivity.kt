package com.example.lab6

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.lab6.utils.Mockups
import java.util.*

class EditEventActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    private lateinit var map: ImageView
    private lateinit var tools: ImageView
    private lateinit var name: EditText
    private lateinit var description: EditText
    private lateinit var save: Button
    private lateinit var back: Button
    private lateinit var reserveTools: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_event)

        map = findViewById(R.id.map)
        tools = findViewById(R.id.tools_img)
        name = findViewById(R.id.name_text)
        description = findViewById(R.id.description_text)
        save = findViewById(R.id.save_button)
        back = findViewById(R.id.back_button)
        reserveTools = findViewById(R.id.tools_img)

        map.setOnClickListener() {
            val intent = Intent(this, BookLocationTimeActivity::class.java)

            startActivity(intent)
        }
        tools.setOnClickListener() {

        }
        save.setOnClickListener() {
            Mockups.eventName = name.text.toString()
            Mockups.eventDescription = description.text.toString()
            Toast.makeText(this, "Changes were saved!", Toast.LENGTH_SHORT).show()

        }
        back.setOnClickListener() {
            val intent = Intent(this, OwnerEventViewActivity::class.java)
            startActivity(intent)
        }
        if (Mockups.eventName != "") {
            name.setText(Mockups.eventName)
        }
        if (Mockups.eventDescription != "") {
            description.setText(Mockups.eventDescription)
        }

        reserveTools.setOnClickListener {
            val intent = Intent(this, Equipment::class.java)
            startActivity(intent)
        }
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("Not yet implemented")
    }
}