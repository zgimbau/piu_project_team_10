package com.example.lab6

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.lab6.utils.Mockups
import com.google.android.material.bottomnavigation.BottomNavigationView

class FeedActivity : AppCompatActivity() {


    private lateinit var likeButton1: ImageView
    private lateinit var goingButton1: ImageView
    private lateinit var goingButton2: ImageView
    private lateinit var goingButton3: ImageView
    private lateinit var likeButton2: ImageView
    private lateinit var commentButton2: ImageView
    private lateinit var commentSection2: LinearLayout
    private lateinit var addCommentButton2: ImageView
    private lateinit var addCommentEditText2: EditText
    private lateinit var commentThree: CardView
    private lateinit var likeButton3: ImageView
    private lateinit var myComment: TextView
    private lateinit var vec1: IntArray
    private lateinit var vec2: IntArray
    private lateinit var vec3: IntArray
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var crossImage: ImageView
    private lateinit var basketImage: ImageView
    private lateinit var cardView: androidx.cardview.widget.CardView
    private lateinit var eventName: TextView
    private lateinit var middleEventImageTwo: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feed_page)



        middleEventImageTwo = findViewById(R.id.middle_event_image_2)

        goingButton1 = findViewById(R.id.bookmark_image_view_1)
        goingButton2 = findViewById(R.id.bookmark_image_view_2)
        goingButton3 = findViewById(R.id.bookmark_image_view_3)

        eventName = findViewById(R.id.title_text_view_3)

        likeButton1 = findViewById(R.id.like_image_view_1)
        likeButton2 = findViewById(R.id.like_image_view_2)
        commentButton2 = findViewById(R.id.add_comment_image_view_2)
        commentSection2 = findViewById(R.id.comments_layout)
        addCommentButton2 = findViewById(R.id.add_comment_button)
        addCommentEditText2 = findViewById(R.id.add_comment_edit_text)
        myComment = findViewById(R.id.comment_three_text)
        commentThree = findViewById(R.id.comment_three)
        crossImage = findViewById(R.id.middle_event_image_2)
        basketImage = findViewById(R.id.middle_event_image_3)
        cardView = findViewById(R.id.background_image_card_view_3)

        likeButton3 = findViewById(R.id.like_image_view_3)
        vec1 = intArrayOf(0, 0, 0)
        vec2 = intArrayOf(0, 0, 0)
        vec3 = intArrayOf(0, 0, 0)

        bottomNavigation = findViewById(R.id.bottom_nav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)


        eventName.setText(Mockups.eventName)

        if (Mockups.owner_event == 0) {
            cardView.visibility = View.GONE;
        } else {
            cardView.visibility = View.VISIBLE;
        }

        likeButton1.setOnClickListener() {
            if (vec1[0] == 0) {
                likeButton1.setImageResource(R.drawable.ic_baseline_favorite_24)
                vec1[0] = 1
            } else {
                likeButton1.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                vec1[0] = 0
            }
        }
        likeButton2.setOnClickListener() {
            if (vec2[0] == 0) {
                likeButton2.setImageResource(R.drawable.ic_baseline_favorite_24)
                vec2[0] = 1
            } else {
                likeButton2.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                vec2[0] = 0
            }
        }
        likeButton3.setOnClickListener() {
            if (vec3[0] == 0) {
                likeButton3.setImageResource(R.drawable.ic_baseline_favorite_24)
                vec3[0] = 1
            } else {
                likeButton3.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                vec3[0] = 0
            }
        }

        commentButton2.setOnClickListener() {
            if (vec2[1] == 0) {
                commentSection2.visibility = View.VISIBLE
                vec2[1] = 1
            } else {
                commentSection2.visibility = View.GONE
                vec2[1] = 0
            }
        }

        addCommentButton2.setOnClickListener() {
            var text = addCommentEditText2.text.toString()
            addCommentEditText2.setText("")
            if (text.length != 0) {
                myComment.text = text
                commentThree.visibility = View.VISIBLE
            }
        }

        goingButton1.setOnClickListener() {
            if (vec1[2] == 0) {
                goingButton1.setImageResource(R.drawable.ic_baseline_bookmark_24)
                vec1[2] = 1
                Toast.makeText(this, "Going to event!", Toast.LENGTH_SHORT).show()
            } else {
                goingButton1.setImageResource(R.drawable.ic_baseline_bookmark_border_24)
                vec1[2] = 0
                Toast.makeText(this, "Not going!", Toast.LENGTH_SHORT).show()
            }
        }
        goingButton2.setOnClickListener() {
            if (vec2[2] == 0) {
                goingButton2.setImageResource(R.drawable.ic_baseline_bookmark_24)
                vec2[2] = 1
                Toast.makeText(this, "Going to event!", Toast.LENGTH_SHORT).show()
            } else {
                goingButton2.setImageResource(R.drawable.ic_baseline_bookmark_border_24)
                vec2[2] = 0
                Toast.makeText(this, "Not going!", Toast.LENGTH_SHORT).show()
            }
        }
        goingButton3.setOnClickListener() {
            if (vec3[2] == 0) {
                goingButton3.setImageResource(R.drawable.ic_baseline_bookmark_24)
                vec3[2] = 1
                Toast.makeText(this, "Going to event!", Toast.LENGTH_SHORT).show()
            } else {
                goingButton3.setImageResource(R.drawable.ic_baseline_bookmark_border_24)
                vec3[2] = 0
                Toast.makeText(this, "Not going!", Toast.LENGTH_SHORT).show()
            }
        }
        crossImage.setOnClickListener() {
            val intent = Intent(this, EventViewPageActivity::class.java)
            startActivity(intent);

        }
        /* basketImage.setOnClickListener(){
             val intent = Intent(this, OwnerEventViewActivity::class.java)
             startActivity(intent);

         }*/

        middleEventImageTwo.setOnClickListener(mOnEventClickListener)

    }

    private val mOnEventClickListener = View.OnClickListener { item ->
        when (item.id) {
            R.id.middle_event_image_2 -> {
                val intent = Intent(this, EventViewPageActivity::class.java)
                startActivity(intent);
            }
        }
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottom_nav_profile -> {
                    val intent = Intent(this, OwnProfileActivity::class.java)
                    startActivity(intent);
                }
                R.id.bottom_nav_feed -> {
                    val intent = Intent(this, FeedActivity::class.java)
                    startActivity(intent)
                }
                R.id.bottom_nav_calendar -> {
                    val intent = Intent(this, EventCalendar::class.java)
                    startActivity(intent);
                }
                R.id.bottom_nav_chat -> {
                    val intent = Intent(this, Friends::class.java)
                    startActivity(intent)
                }
            }
            false
        }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure you want to sign out?")
            .setPositiveButton("Yes") { _, _ ->finish()}
            .setNegativeButton("No") { _, _ -> }
        builder.create().show()


    }

}