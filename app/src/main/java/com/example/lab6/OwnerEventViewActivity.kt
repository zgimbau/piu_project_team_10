package com.example.lab6

import android.content.ClipDescription
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.lab6.utils.Mockups
import com.google.android.material.bottomnavigation.BottomNavigationView

class OwnerEventViewActivity: AppCompatActivity(), AdapterView.OnItemClickListener {
    private lateinit var editButton: Button
    private lateinit var shareButton: Button
    private lateinit var deleteButton: Button
    private lateinit var name:TextView
    private lateinit var description: TextView

    private lateinit var vec1: IntArray
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.owner_event_view)

        editButton=findViewById(R.id.edit_button)
        shareButton=findViewById(R.id.share_button)
        deleteButton=findViewById(R.id.delete_button)
        name=findViewById(R.id.event_title_text_view)
        description=findViewById(R.id.description_text)
        vec1 = intArrayOf(0,0)

        editButton.setOnClickListener(){
            val intent  = Intent(this, EditEventActivity::class.java)

            startActivity(intent)
        }
        shareButton.setOnClickListener(){
            Toast.makeText(this,"The event was shared !", Toast.LENGTH_SHORT).show()
        }
        deleteButton.setOnClickListener(){
            Mockups.owner_event=0
            val intent  = Intent(this, OwnProfileActivity::class.java)

            startActivity(intent)
        }
        if(Mockups.eventName != ""){
            name.setText(Mockups.eventName)
        }
        if(Mockups.eventDescription != ""){
            description.setText(Mockups.eventDescription)
        }

    }
    /*private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.delete_button ->{
                Mockups.owner_event=0
                val intent  = Intent(this, OwnProfileActivity::class.java)

                startActivity(intent)
            }

        }
        false
    }*/
    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("Not yet implemented")
    }
}