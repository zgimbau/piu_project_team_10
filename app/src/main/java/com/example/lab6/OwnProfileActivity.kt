package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.lab6.utils.Mockups
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.w3c.dom.Text


class OwnProfileActivity : AppCompatActivity() {

    private lateinit var selectActivitiesLayout: LinearLayout
    private lateinit var ownActivitiesLayout: LinearLayout
    private lateinit var editProfileButton: Button
    private lateinit var createEventButton: Button

    private lateinit var rowingIcon: ImageView
    private lateinit var yogaIcon: ImageView
    private lateinit var snowboardIcon: ImageView
    private lateinit var bikingIcon: ImageView
    private lateinit var hikingIcon: ImageView
    private lateinit var weightliftingIcon: ImageView

    private lateinit var rowingSelectIcon: ImageView
    private lateinit var yogaSelectIcon: ImageView
    private lateinit var snowboardSelectIcon: ImageView
    private lateinit var bikingSelectIcon: ImageView
    private lateinit var hikingSelectIcon: ImageView
    private lateinit var weightliftingSelectIcon: ImageView
    private lateinit var username: TextView
    private lateinit var eventImage: ImageView

    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var cardView: androidx.constraintlayout.widget.ConstraintLayout
    private lateinit var crossEvent: ImageView
    private lateinit var basketEvent: ImageView
    private lateinit var newEventCardView: ConstraintLayout
    private lateinit var newEventName: TextView

    private lateinit var name: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.own_profile)

        username = findViewById(R.id.own_profile_username)
        eventImage = findViewById(R.id.own_event_two_cover_image)
        selectActivitiesLayout = findViewById(R.id.own_profile_activities_selection_linear_layout)
        ownActivitiesLayout = findViewById(R.id.own_profile_activities_linear_layout)
        editProfileButton = findViewById(R.id.edit_profile_button)
        createEventButton = findViewById(R.id.create_event_button)

        rowingIcon = findViewById(R.id.rowing_act_op)
        yogaIcon = findViewById(R.id.yoga_act_op)
        snowboardIcon = findViewById(R.id.snowboard_act_op)
        bikingIcon = findViewById(R.id.biking_act_op)
        hikingIcon = findViewById(R.id.hiking_act_op)
        weightliftingIcon = findViewById(R.id.weightlifting_act_op)

        rowingSelectIcon = findViewById(R.id.rowing_act_select)
        yogaSelectIcon = findViewById(R.id.yoga_act_select)
        snowboardSelectIcon = findViewById(R.id.snowboard_act_select)
        bikingSelectIcon = findViewById(R.id.biking_act_select)
        hikingSelectIcon = findViewById(R.id.hiking_act_select)
        weightliftingSelectIcon = findViewById(R.id.weightlifting_act_select)
        newEventCardView = findViewById(R.id.own_event_three_constraint_layout)
        newEventName = findViewById(R.id.event_three_name)
        username.text = Mockups.ownName

        bottomNavigation = findViewById(R.id.bottom_nav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        crossEvent = findViewById(R.id.own_event_one_cover_image)
        basketEvent = findViewById(R.id.own_event_two_cover_image)
        cardView = findViewById(R.id.own_event_two_constraint_layout)
        if(Mockups.showNewEvent == 0){
            newEventCardView.visibility = View.GONE
        }
        else{
            var l = Mockups.newEventName
            newEventName.setText(Mockups.newEventName.toString())
            newEventCardView.visibility = View.VISIBLE
        }

        name = findViewById(R.id.img2_name)

        if (Mockups.owner_event == 0) {
            cardView.visibility = View.GONE;
        }
        name.setText(Mockups.eventName)

        if (Mockups.own_activities[0] == 0) {
            rowingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            rowingIcon.visibility = View.GONE
        } else {
            rowingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            rowingIcon.visibility = View.VISIBLE
        }

        if (Mockups.own_activities[1] == 0) {
            yogaSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            yogaIcon.visibility = View.GONE
        } else {
            yogaSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            yogaIcon.visibility = View.VISIBLE
        }

        if (Mockups.own_activities[2] == 0) {
            snowboardSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            snowboardIcon.visibility = View.GONE
        } else {
            snowboardSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            snowboardIcon.visibility = View.VISIBLE
        }

        if (Mockups.own_activities[3] == 0) {
            bikingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            bikingIcon.visibility = View.GONE
        } else {
            bikingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            bikingIcon.visibility = View.VISIBLE
        }

        if (Mockups.own_activities[4] == 0) {
            hikingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            hikingIcon.visibility = View.GONE
        } else {
            hikingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            hikingIcon.visibility = View.VISIBLE
        }

        if (Mockups.own_activities[5] == 0) {
            weightliftingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            weightliftingIcon.visibility = View.GONE
        } else {
            weightliftingSelectIcon.setColorFilter(
                ContextCompat.getColor(this, R.color.yellow),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
            weightliftingIcon.visibility = View.VISIBLE
        }

        ownActivitiesLayout.setOnClickListener {
            if (selectActivitiesLayout.visibility == View.VISIBLE) {
                selectActivitiesLayout.visibility = View.GONE
            } else
                selectActivitiesLayout.visibility = View.VISIBLE
        }

        rowingSelectIcon.setOnClickListener(imageOnClickListener)
        bikingSelectIcon.setOnClickListener(imageOnClickListener)
        yogaSelectIcon.setOnClickListener(imageOnClickListener)
        weightliftingSelectIcon.setOnClickListener(imageOnClickListener)
        hikingSelectIcon.setOnClickListener(imageOnClickListener)
        snowboardSelectIcon.setOnClickListener(imageOnClickListener)
        eventImage.setOnClickListener(imageOnClickListener)

        editProfileButton.setOnClickListener(buttonClickListener)
        createEventButton.setOnClickListener() {
            val intent = Intent(this, CreateEvent::class.java)
            startActivity(intent);
        }
        crossEvent.setOnClickListener() {
            val intent = Intent(this, EventViewPageActivity::class.java)
            startActivity(intent);
        }
        basketEvent.setOnClickListener() {
            val intent = Intent(this, OwnerEventViewActivity::class.java)
            startActivity(intent);
        }


    }

    private val buttonClickListener = View.OnClickListener { item ->
        when (item.id) {
            R.id.edit_profile_button -> {
                val intent = Intent(this, EditProfile::class.java)
                startActivity(intent)
            }
        }
    }

    private val imageOnClickListener = View.OnClickListener { item ->
        when (item.id) {
            R.id.rowing_act_select -> {
                if (Mockups.own_activities[0] == 1) {
                    rowingIcon.visibility = View.GONE
                    rowingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.white),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[0] = 0
                } else {
                    rowingIcon.visibility = View.VISIBLE
                    rowingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.yellow),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[0] = 1
                }
            }
            R.id.yoga_act_select -> {
                if (Mockups.own_activities[1] == 1) {
                    yogaIcon.visibility = View.GONE
                    yogaSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.white),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[1] = 0
                } else {
                    yogaIcon.visibility = View.VISIBLE
                    yogaSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.yellow),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[1] = 1
                }
            }
            R.id.snowboard_act_select -> {
                if (Mockups.own_activities[2] == 1) {
                    snowboardIcon.visibility = View.GONE
                    snowboardSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.white),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[2] = 0
                } else {
                    snowboardIcon.visibility = View.VISIBLE
                    snowboardSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.yellow),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[2] = 1
                }
            }
            R.id.biking_act_select -> {
                if (Mockups.own_activities[3] == 1) {
                    bikingIcon.visibility = View.GONE
                    bikingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.white),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[3] = 0
                } else {
                    bikingIcon.visibility = View.VISIBLE
                    bikingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.yellow),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[3] = 1
                }
            }
            R.id.hiking_act_select -> {
                if (Mockups.own_activities[4] == 1) {
                    hikingIcon.visibility = View.GONE
                    hikingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.white),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[4] = 0
                } else {
                    hikingIcon.visibility = View.VISIBLE
                    hikingSelectIcon.setColorFilter(
                        ContextCompat.getColor(this, R.color.yellow),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[4] = 1
                }
            }
            R.id.weightlifting_act_select -> {
                if (Mockups.own_activities[5] == 1) {
                    weightliftingIcon.visibility = View.GONE
                    weightliftingSelectIcon.setColorFilter(
                        ContextCompat.getColor(
                            this,
                            R.color.white
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[5] = 0
                } else {
                    weightliftingIcon.visibility = View.VISIBLE
                    weightliftingSelectIcon.setColorFilter(
                        ContextCompat.getColor(
                            this,
                            R.color.yellow
                        ), android.graphics.PorterDuff.Mode.SRC_IN
                    )
                    Mockups.own_activities[5] = 1
                }
            }
            R.id.own_event_two_cover_image -> {
                val intent = Intent(this, OwnerEventViewActivity::class.java)
                startActivity(intent)
            }
        }
        false
    }


    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottom_nav_profile -> {
                    val intent = Intent(this, OwnProfileActivity::class.java)
                    startActivity(intent);
                }
                R.id.bottom_nav_feed -> {
                    val intent = Intent(this, FeedActivity::class.java)
                    startActivity(intent)
                }
                R.id.bottom_nav_calendar -> {
                    val intent = Intent(this, EventCalendar::class.java)
                    startActivity(intent)
                }
                R.id.bottom_nav_chat -> {
                    val intent = Intent(this, Friends::class.java)
                    startActivity(intent)
                }
            }
            false
        }

    /* private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
         when(item.itemId){
             R.id.own_event_one_cover_image ->{
                 val intent = Intent(this, EventViewPageActivity::class.java)
                 startActivity(intent);
             }
             R.id.own_event_two_cover_image ->{
                 val intent = Intent(this, OwnerEventViewActivity::class.java)
                 startActivity(intent);
             }
         }
         false
     }*/

}