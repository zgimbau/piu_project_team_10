package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class ReserveTools : AppCompatActivity() {
    private lateinit var payButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reserve_tools)

        payButton = findViewById(R.id.pay_button)

        payButton.setOnClickListener() {
            Toast.makeText(this, "Payment complete!", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}