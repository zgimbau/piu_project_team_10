package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import java.util.*
import kotlin.concurrent.schedule

class GoogleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google)

        Timer("GoogleSignUp", false).schedule(2000){
            Handler(mainLooper).post{
                val intent = Intent(this@GoogleActivity, FeedActivity::class.java)
                startActivity(intent)
            }
        }
    }
}