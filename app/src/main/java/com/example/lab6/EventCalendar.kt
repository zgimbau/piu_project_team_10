package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView

class EventCalendar : AppCompatActivity() {

    private lateinit var bottomNavigation: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_calendar)

        bottomNavigation = findViewById(R.id.bottom_nav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottom_nav_profile -> {
                    val intent = Intent(this, OwnProfileActivity::class.java)
                    startActivity(intent);
                }
                R.id.bottom_nav_feed -> {
                    val intent = Intent(this, FeedActivity::class.java)
                    startActivity(intent)
                }
                R.id.bottom_nav_calendar -> {
                    val intent = Intent(this, EventCalendar::class.java)
                    startActivity(intent)
                }
                R.id.bottom_nav_chat -> {
                    val intent = Intent(this, Friends::class.java)
                    startActivity(intent)
                }
            }
            false
        }
}