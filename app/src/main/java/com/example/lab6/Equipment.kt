package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Equipment : AppCompatActivity() {

    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_equipment)

        button = findViewById(R.id.description_text_product2)
        button.setOnClickListener() {
            val intent = Intent(this, ReserveTools::class.java)
            startActivity(intent)
            finish()
        }
    }
}