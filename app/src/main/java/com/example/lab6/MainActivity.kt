package com.example.lab6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.lab6.utils.Mockups
import kotlin.math.sign

class MainActivity : AppCompatActivity() {

    private lateinit var errorUsername: TextView
    private lateinit var errorPassword: TextView
    private lateinit var submitMessage: TextView
    private lateinit var singInButton: Button
    private lateinit var signUpButton: Button
    private lateinit var facebookLogin: ImageView
    private lateinit var googleLogin: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        errorUsername = findViewById(R.id.usernameErrorLabel)
        errorPassword = findViewById(R.id.passwordErrorLabel)
        submitMessage = findViewById(R.id.loginInfoLabel)
        singInButton = findViewById(R.id.singInButton)
        signUpButton = findViewById(R.id.singUpButton)
        facebookLogin = findViewById(R.id.fb)
        googleLogin = findViewById(R.id.google)

        singInButton.setOnClickListener() {
            val usernameText: EditText = findViewById(R.id.usernameText)
            val passwordText: EditText = findViewById(R.id.passwordText)
            val message: TextView = findViewById(R.id.loginInfoLabel)
            var isOk = false

            if (usernameText.text.isEmpty() || usernameText.text.length < 3) {

                errorUsername.visibility = View.VISIBLE
                isOk = false
            } else {
                errorUsername.visibility = View.GONE
                isOk = true
            }

            if (passwordText.text.isEmpty() || passwordText.text.length < 3) {
                errorPassword.visibility = View.VISIBLE
                isOk = false
            } else {
                errorPassword.visibility = View.GONE
                isOk = true
            }

            if (isOk) {
                if ((usernameText.text.toString() == "admin" && passwordText.text.toString() == "admin1") ||
                    (Mockups.newUserFullName.isNotEmpty() && usernameText.text.toString() == Mockups.newUserUsername && passwordText.text.toString() == Mockups.newUserPassword)
                ) {
                    message.text = getString(R.string.loginSuccessful)
                    message.setTextColor(getColor(R.color.green))
                    message.visibility = View.VISIBLE

                    val intent = Intent(this, FeedActivity::class.java)
                    startActivity(intent);
                } else {
                    message.text = getString(R.string.loginFailed)
                    message.setTextColor(getColor(R.color.red))
                    message.visibility = View.VISIBLE
                }
            }
        }

        signUpButton.setOnClickListener(mOnButtonClickListener)
        facebookLogin.setOnClickListener(mOnButtonClickListener)
        googleLogin.setOnClickListener(mOnButtonClickListener)
    }

    private var mOnButtonClickListener = View.OnClickListener { item ->
        when (item.id) {
            R.id.singUpButton -> {
                val intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }
            R.id.fb -> {
                val intent = Intent(this, FacebookLoginActivity::class.java)
                startActivity(intent)
            }
            R.id.google -> {
                val intent = Intent(this, GoogleActivity::class.java)
                startActivity(intent)
            }
        }
    }
}

