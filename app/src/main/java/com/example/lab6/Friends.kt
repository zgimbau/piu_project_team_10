package com.example.lab6

import android.content.Intent
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout

class Friends : AppCompatActivity() {
    private lateinit var person: LinearLayout
    private lateinit var backImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        backImage = findViewById(R.id.back_image)
        person = findViewById(R.id.person1)

        person.setOnClickListener() {
            val intent = Intent(this, Chat::class.java)
            startActivity(intent)
        }

        backImage.setOnClickListener{
            val intent = Intent(this, FeedActivity::class.java)
            startActivity(intent)
        }
    }
}