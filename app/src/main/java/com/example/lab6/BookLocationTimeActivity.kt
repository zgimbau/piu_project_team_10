package com.example.lab6

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class BookLocationTimeActivity : AppCompatActivity(), AdapterView.OnItemClickListener {
    private lateinit var mPickTimeBtn: Button
    private lateinit var textView: TextView
    private lateinit var book: Button
    private lateinit var program: TextView

    private lateinit var map: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.book_location_time)

        mPickTimeBtn = findViewById(R.id.pickDateBtn)
        textView = findViewById(R.id.dateTv)
        map = findViewById(R.id.map)
        book = findViewById(R.id.book_button)
        program = findViewById(R.id.program_text)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        mPickTimeBtn.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                textView.setText("" + dayOfMonth + "," + month+1 + "," + year)
            }, year, month, day)
            dpd.show()

        }
        map.setOnClickListener() {
            program.setText("daily between 9AM-11AM")
            Toast.makeText(this, "Location selected!", Toast.LENGTH_SHORT).show()
        }
        book.setOnClickListener() {
            Toast.makeText(this, "Location booked!", Toast.LENGTH_SHORT).show()
        }


    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("Not yet implemented")
    }
}